
let n = 1;

function getString() {
    return n + ' fois'
}

function render () {

    let tags = [
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];

    const tagsEl = tags.map((tag, index) => {
        return <li key={index}>{tag}</li>
    });

    const title = <React.Fragment>
        <h1 id={"title-"+n} className={"class-" + n}>Compteur : <span>{n}</span></h1>
        <ul>{tagsEl}</ul>
    </React.Fragment>;

    ReactDOM.render(title, document.querySelector('#app'));

}

render();

setInterval(() => {
    n++;
    render();
}, 1000);



