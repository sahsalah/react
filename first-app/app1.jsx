function CompteurFn(props) {
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

class Compteur extends React.Component {

    render() {
        return <div>
            <h1>Compteur : <span>{this.props.name}</span></h1>
            <p>{this.props.children}</p>
        </div>
    }
}

class Accueil extends React.Component {
    render() {
        return <div>
            <CompteurFn name="Jean"/>
            <Compteur name="Sam">Il s'appelle Sam</Compteur>
        </div>
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
