function CompteurFn1(props) {
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

class Compteur extends React.Component {

    constructor(props) {
        super(props);
        console.log('1');
        this.state = {ok: '1'}
    }

    static getDerivedStateFromProps(state) {
        console.log('2');
        return state;
    }

    render() {
        console.log('3');
        return <div>
            <h1>Compteur : <span>{this.props.name}</span></h1>
            <p>{this.props.children}</p>
        </div>
    }

    componentDidMount() {
        console.log('4');
    }

    componentDidUpdate() {
        console.log('5');
    }

    componentWillUnmount() {
        console.log('6');
    }

}

class Accueil extends React.Component {
    render() {
        return <div>
            <CompteurFn1 name="Jean"/>
            <Compteur name="Sam">Il s'appelle Sam</Compteur>
        </div>
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
